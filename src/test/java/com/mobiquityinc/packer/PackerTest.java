package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Combinations;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.mobiquityinc.packer.TestFactory.getExpectedCombinations;
import static com.mobiquityinc.packer.TestFactory.getExpectedPackages;
import static com.mobiquityinc.packer.Util.CombinationsGenerator.combine;
import static com.mobiquityinc.packer.Util.MoneyUtil.eur;
import static com.mobiquityinc.packer.Util.MoneyUtil.money;
import static com.mobiquityinc.packer.Util.PackageConverter.convertFileToPackages;
import static org.junit.Assert.assertEquals;

public class PackerTest {

    private static final String TEST_FILE_PATH = "InputFileTest.txt";
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }


    @Test(expected = APIException.class)
    public void createPackage_OverweightTest() throws APIException {
        Package aPackage = new Package();
        aPackage.createPackage(200, Collections.emptyList());
    }

    @Test(expected = APIException.class)
    public void createItem_OverweightTest() throws APIException {
        Item.createItem(1, 150, money(10, eur().getCurrencyCode()));
    }

    @Test(expected = APIException.class)
    public void createItem_CostExceedsLimitTest() throws APIException {
        Item.createItem(1, 80, money(102, eur().getCurrencyCode()));
    }

    @Test
    public void getAllCombinationsTest() {
        List<Item> possibleItems = new ArrayList<>();
        Item item1 = new Item(1, 20.00, money(10.00, eur().getCurrencyCode()));
        Item item2 = new Item(2, 30.00, money(20.00, eur().getCurrencyCode()));
        possibleItems.add(item1);
        possibleItems.add(item2);
        List<Combinations> actualCombinations = combine(possibleItems);
        List<Combinations> expectedCombinations = getExpectedCombinations(item1, item2);

        assertEquals(expectedCombinations, actualCombinations);
    }

    @Test
    public void collectPackagesToBePackedTest() throws IOException, URISyntaxException {
        List<Package> actualPackages = convertFileToPackages(TEST_FILE_PATH);
        List<Package> expectedPackages = getExpectedPackages();

        assertEquals(expectedPackages, actualPackages);
    }

    @Test
    public void filerOutCombinationsExceedingPackageMaxWeightTest() throws APIException {
        List<Combinations> actualEligibleCombinations = TestFactory.getActualEligibleCombinations();
        List<Combinations> expectedEligibleCombinations = TestFactory.getExpectedEligibleCombinations();

        assertEquals(expectedEligibleCombinations, actualEligibleCombinations);
    }

    @Test
    public void packTest() throws IOException, URISyntaxException {
        Packer.pack(TEST_FILE_PATH);
        assertEquals("-\n" +
                "-\n" +
                "2,3\n", outContent.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
}
