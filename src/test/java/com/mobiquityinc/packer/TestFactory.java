package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Combinations;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.packer.Util.PackageConverter;

import java.util.ArrayList;
import java.util.List;

import static com.mobiquityinc.packer.Util.MoneyUtil.money;

class TestFactory {

    static List<Combinations> getExpectedCombinations(Item item1, Item item2) {
        List<Item> combination1 = new ArrayList<>();
        List<Item> combination2 = new ArrayList<>();
        List<Item> combination3 = new ArrayList<>();
        combination1.add(item1);
        combination2.add(item1);
        combination2.add(item2);
        combination3.add(item2);
        List<Combinations> expectedCombinations = new ArrayList<>();
        Combinations combinations1 = new Combinations(combination1, money(10.00, "EUR"), 20);
        Combinations combinations2 = new Combinations(combination2, money(30.00, "EUR"), 50);
        Combinations combinations3 = new Combinations(combination3, money(20.00, "EUR"), 30);
        expectedCombinations.add(combinations1);
        expectedCombinations.add(combinations2);
        expectedCombinations.add(combinations3);

        return expectedCombinations;
    }

    static List<Package> getExpectedPackages() {
        List<Item> package1Items = new ArrayList<>();

        Item package1Item1 = new Item(1, 53.38, money(45.00, "EUR"));
        Item package1Item2 = new Item(2, 88.62, money(98.00, "EUR"));
        package1Items.add(package1Item1);
        package1Items.add(package1Item2);

        Package aPackage1 = new Package(50.00, package1Items);

        List<Item> package2Items = new ArrayList<>();

        Item package2Item1 = new Item(1, 15.3, money(34.00, "EUR"));
        package2Items.add(package2Item1);

        Package aPackage2 = new Package(8.00, package2Items);

        List<Item> package3Items = new ArrayList<>();

        Item package3Item1 = new Item(1, 85.31, money(29.00, "EUR"));
        Item package3Item2 = new Item(2, 14.55, money(74.00, "EUR"));
        Item package3Item3 = new Item(3, 3.98, money(16.00, "EUR"));
        package3Items.add(package3Item1);
        package3Items.add(package3Item2);
        package3Items.add(package3Item3);

        Package aPackage3 = new Package(75.00, package3Items);

        List<Package> expectedPackages = new ArrayList<>();
        expectedPackages.add(aPackage1);
        expectedPackages.add(aPackage2);
        expectedPackages.add(aPackage3);
        return expectedPackages;
    }

    static List<Combinations> getActualEligibleCombinations() throws APIException {

        return PackageConverter.filerOutCombinationsExceedingPackageMaxWeight(80.00, getAllCombinations());
    }

    private static List<Combinations> getAllCombinations() throws APIException {
        List<Combinations> combinations = new ArrayList<>();
        combinations.add(getCombination1());
        combinations.add(getCombination2());
        combinations.add(getCombination3());
        return combinations;
    }

    private static Combinations getCombination3() throws APIException {
        List<Item> combinations3Item = new ArrayList<>();

        Item combinations3Item1 = Item.createItem(1, 40, money(30.00, "EUR"));
        Item combinations3Item2 = Item.createItem(2, 30, money(30.00, "EUR"));
        Item combinations3Item3 = Item.createItem(3, 10, money(30.00, "EUR"));
        combinations3Item.add(combinations3Item1);
        combinations3Item.add(combinations3Item2);
        combinations3Item.add(combinations3Item3);
        return new Combinations(combinations3Item);
    }

    private static Combinations getCombination2() throws APIException {
        List<Item> combinations2Item = new ArrayList<>();
        Item combinations2Item1 = Item.createItem(1, 20, money(30.00, "EUR"));
        Item combinations2Item2 = Item.createItem(2, 30, money(30.00, "EUR"));
        Item combinations2Item3 = Item.createItem(3, 70, money(30.00, "EUR"));
        combinations2Item.add(combinations2Item1);
        combinations2Item.add(combinations2Item2);
        combinations2Item.add(combinations2Item3);
        return new Combinations(combinations2Item);
    }

    private static Combinations getCombination1() throws APIException {
        List<Item> combinations1Item = new ArrayList<>();
        Item combinations1Item1 = Item.createItem(1, 10, money(30.00, "EUR"));
        Item combinations1Item2 = Item.createItem(2, 30, money(30.00, "EUR"));
        Item combinations1Item3 = Item.createItem(3, 40, money(30.00, "EUR"));

        combinations1Item.add(combinations1Item1);
        combinations1Item.add(combinations1Item2);
        combinations1Item.add(combinations1Item3);
        return new Combinations(combinations1Item);
    }

    static List<Combinations> getExpectedEligibleCombinations() throws APIException {
        List<Combinations> combinations = new ArrayList<>();
        combinations.add(getCombination1());
        combinations.add(getCombination3());
        return combinations;
    }

}
