package com.mobiquityinc.model;

import com.mobiquityinc.exception.APIException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Package {
    private double maxWeight = 100;
    private List<Item> items;
    static final Logger logger = LoggerFactory.getLogger(Package.class);


    public Package createPackage(double maxWeight, List<Item> items) throws APIException {
        if (maxWeight > 100) {
            throw new APIException("Weight cannot exceed 100, ..Skipping package");
        }
        this.maxWeight = maxWeight <= 100 ? maxWeight : this.maxWeight;
        return new Package(this.maxWeight, items);
    }

    public static double getMaxWeight(String line) {
        return Double.parseDouble(StringUtils.substringBefore(line, ":"));
    }
}
