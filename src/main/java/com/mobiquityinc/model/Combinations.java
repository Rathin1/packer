package com.mobiquityinc.model;

import com.mobiquityinc.exception.APIException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.money.MonetaryAmount;
import java.util.ArrayList;
import java.util.List;

import static com.mobiquityinc.packer.Util.MoneyUtil.zero_eur;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Combinations {

    private List<Item> items;
    private MonetaryAmount totalCost;
    private double totalWeight;

    private static final Logger logger = LoggerFactory.getLogger(Package.class);

    public Combinations(List<Item> items) {
        this.items = new ArrayList<>();
        for (Item item : items) {
            try {
                this.items.add(Item.createItem(item.getIndexNumber(), item.getWeight(), item.getCost()));
            } catch (APIException apiException) {
                logger.error(apiException.getMessage());
            } catch (NullPointerException e) {
                continue;
            }
        }
    }

    public MonetaryAmount getTotalCost() {
        this.totalCost = zero_eur();
        for (Item item : this.items) {
            this.totalCost = this.totalCost.add(item.getCost());
        }
        return this.totalCost;
    }

    public double getTotalWeight() {
        this.totalWeight = 0;
        for (Item item : this.items) {
            this.totalWeight += item.getWeight();
        }
        return this.totalWeight;
    }
}
