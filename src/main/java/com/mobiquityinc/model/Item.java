package com.mobiquityinc.model;

import com.mobiquityinc.exception.APIException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mobiquityinc.model.Constants.MAX_COST;
import static com.mobiquityinc.model.Constants.MAX_WEIGHT;
import static com.mobiquityinc.packer.Util.MoneyUtil.eur;
import static com.mobiquityinc.packer.Util.MoneyUtil.money;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    private int indexNumber;
    private double weight;
    private Money cost;

    private static final Logger logger = LoggerFactory.getLogger(Item.class);

    public static List<Item> getItemList(String line) {
        List<Item> items = new ArrayList<>();
        String itemsOnLine = StringUtils.substringAfter(line, ":");
        Pattern pattern = Pattern.compile("\\((.*?)\\)");
        Matcher matcher = pattern.matcher(itemsOnLine);

        while (matcher.find()) {
            String singleItem = matcher.group(0);
            String valuesBetweenBrackets = StringUtils.substringBetween(singleItem, "(", ")");
            String[] itemAttributes = StringUtils.split(valuesBetweenBrackets, ", ");
            Item item = null;
            try {
                item = createItem(convertIndex(itemAttributes[0]), convertWeight(itemAttributes[1]), extractCost(itemAttributes[2]));
            } catch (APIException e) {
                logger.error(e.getMessage());
            }
            items.add(item);
        }
        return items;
    }

    private static int convertIndex(String index) {
        return Integer.parseInt(index);
    }

    private static double convertWeight(String weight) {
        return Double.parseDouble(weight);
    }

    private static Money extractCost(String cost) {
        return money(Double.parseDouble(StringUtils.substringAfter(cost, "€")), eur().getCurrencyCode());
    }

    public static Item createItem(int indexNumber, double weight, Money cost) throws APIException {
        if (weight > MAX_WEIGHT) {
            throw new APIException("Weight cannot exceed " + MAX_WEIGHT + "...Skipping item!");
        }
        if (cost.isGreaterThan(MAX_COST)) {
            throw new APIException("Cost cannot exceed " + MAX_COST + "...Skipping item!");
        }
        return new Item(indexNumber, weight, cost);
    }
}
