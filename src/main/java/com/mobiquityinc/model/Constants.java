package com.mobiquityinc.model;

import org.javamoney.moneta.Money;

import static com.mobiquityinc.packer.Util.MoneyUtil.eur;

class Constants {
    static final int MAX_WEIGHT = 100;
    static final Money MAX_COST = Money.of(100, eur().getCurrencyCode());
}
