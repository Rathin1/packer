package com.mobiquityinc.packer;

import com.mobiquityinc.model.Combinations;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.packer.Util.PackageConverter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static com.mobiquityinc.packer.Util.CombinationsGenerator.combine;
import static com.mobiquityinc.packer.Util.PackageConverter.filerOutCombinationsExceedingPackageMaxWeight;

public class Packer {

    public static void main(String[] args) throws IOException, URISyntaxException {
        pack("InputFile.txt");
    }

    static void pack(String filePath) throws URISyntaxException, IOException {
        //Get Packages from File
        List<Package> packagesToPack = PackageConverter.convertFileToPackages(filePath);
        //Iterates over packages and pack items
        for (Package aPackage : packagesToPack) {
            //Obtain all possible combinations of items that fits in package
            List<Combinations> combinations = combine(aPackage.getItems());

            List<Combinations> eligibleCombinations = filerOutCombinationsExceedingPackageMaxWeight(aPackage.getMaxWeight(), combinations);

            if (!eligibleCombinations.isEmpty()) {
                System.out.println(
                        eligibleCombinations.get(0).getItems()
                                .stream()
                                .map(item -> String.valueOf(item.getIndexNumber())).
                                collect(Collectors.joining(",")));
            } else {
                System.out.println("-");
            }
        }
    }

}
