package com.mobiquityinc.packer.Util;

import com.mobiquityinc.model.Combinations;
import com.mobiquityinc.model.Item;

import java.util.ArrayList;
import java.util.List;

public class CombinationsGenerator {

    public static List<Combinations> combine(List<Item> items) {
        int length = items.size();

        List<Combinations> combinations = new ArrayList<>();
        List<Item> combination = new ArrayList<>();

        combination(items, length, combination, combinations, 0);
        return combinations;
    }

    //Find all possible combinations based on items
    private static void combination(List<Item> possibleItems, int length, List<Item> items, List<Combinations> output, int level) {
        if (level == length) {
            return;
        } else {
            for (int i = level; i < length; i++) {
                items.add(possibleItems.get(i));
                output.add(new Combinations(items));
                combination(possibleItems, length, items, output, i + 1);
                items.remove(items.size() - 1);
            }
        }
    }

}
