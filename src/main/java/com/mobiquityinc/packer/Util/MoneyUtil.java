package com.mobiquityinc.packer.Util;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

public class MoneyUtil {

    private static final String EURO = "EUR";

    public static Money money(double amount, String currency) {
        return Money.of(amount, currency);
    }

    public static CurrencyUnit eur() {
        return Monetary.getCurrency(EURO);
    }

    public static Money zero_eur() {
        return money(0, EURO);
    }


}
