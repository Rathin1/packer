package com.mobiquityinc.packer.Util;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Combinations;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mobiquityinc.model.Item.getItemList;
import static com.mobiquityinc.model.Package.getMaxWeight;

public class PackageConverter {
    static final Logger logger = LoggerFactory.getLogger(Package.class);

    public static List<Package> convertFileToPackages(String filePath) throws URISyntaxException, IOException {
        Path path = Paths.get(Objects.requireNonNull(PackageConverter.class.getClassLoader()
                .getResource(filePath)).toURI());
        return collectPackagesToBePacked(path);
    }

    private static List<Package> collectPackagesToBePacked(Path path) throws IOException {
        Stream<String> lines = Files.lines(path);
        List<Package> packages = new ArrayList<>();
        lines.forEach(line -> {
            List<Item> items = getItemList(line);
            Package aPackage = new Package();
            try {
                packages.add((aPackage.createPackage(getMaxWeight(line), items)));
            } catch (APIException e) {
                logger.error(e.getMessage());
            }
        });
        lines.close();
        return packages;
    }

    public static List<Combinations> filerOutCombinationsExceedingPackageMaxWeight(double maxWeight, List<Combinations> combinations) {
        return combinations.stream()
                .filter(items1 -> items1.getItems().stream().mapToDouble(Item::getWeight).sum() <= maxWeight)
                .sorted(Comparator.comparing(Combinations::getTotalCost).reversed().thenComparing(Combinations::getTotalWeight))
                .collect(Collectors.toList());
    }

}
